package me.cbartlett.minemanager;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.List;

public class ConnectSuccessActivity extends AppCompatActivity implements SensorEventListener{


    private ListView mDrawerList;
    private ArrayAdapter<String> mAdapter;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private String mActivityTitle;

    private SensorManager sm;
    private Sensor acc;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect_success);

        sm = (SensorManager) getSystemService(SENSOR_SERVICE);
        acc = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sm.registerListener(this, acc, SensorManager.SENSOR_DELAY_NORMAL);

        mDrawerList = (ListView)findViewById(R.id.navList);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);

        Toast.makeText(this, "Login Successful", Toast.LENGTH_SHORT).show();

        addDrawerItems();
        setUpDrawer();
        registerDrawerClicks();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        mActivityTitle = getTitle().toString();


        DataManagerSingleton.getInstance().setConnectSuccessActivityContext(this);

        //IntentFilter if1 = new IntentFilter(ConnectionService.BROADCAST_FILTER);
        //this.registerReceiver(DataManagerSingleton.getInstance().getConnectionService().broadcastReceiver, if1);


        Intent localIntent = new Intent().setAction(ConnectionService.BROADCAST_FILTER).putExtra("COMMAND", "execute");
        sendBroadcast(localIntent);


        ImageView im = (ImageView) findViewById(R.id.imageView);
        Picasso.with(this).load("https://crafatar.com/avatars/4f80d7f9-664a-41aa-9b26-7922b8ba8c5a.png").into(im);

        android.support.v4.app.NotificationCompat.Builder ncb = new NotificationCompat.Builder(this).setSmallIcon(R.drawable.minecrafticon).setContentTitle("MineManager").setContentText("Admin Tool Connected").setSubText("Christopher Bartlett - 40220096").setOngoing(true);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(ConnectSuccessActivity.class);
        stackBuilder.addNextIntent(new Intent());
        PendingIntent resultPI = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        ncb.setContentIntent(resultPI);
        NotificationManager mNM = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNM.notify(10, ncb.build());

    }
    @Override
    protected void onResume() {
        super.onResume();
        sm.registerListener(this, sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
        Intent localIntent = new Intent().setAction(ConnectionService.BROADCAST_FILTER).putExtra("COMMAND", "execute");
        sendBroadcast(localIntent);
    }

    @Override
    protected void onPause() {
        sm.unregisterListener(this);
        super.onPause();
    }

    private void registerDrawerClicks(){
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                Object o = mDrawerList.getItemAtPosition(position);
                if(o.toString().equalsIgnoreCase("Players")){
                    Intent localIntent = new Intent().setAction(ConnectionService.BROADCAST_FILTER).putExtra("COMMAND", "player_details_request");
                    sendBroadcast(localIntent);
                    Intent i = new Intent(ConnectSuccessActivity.this, PLActivity.class);
                    startActivity(i);
                }

                if(o.toString().equalsIgnoreCase("Console Command")){
                    AlertDialog.Builder alert = new AlertDialog.Builder(ConnectSuccessActivity.this);

                    alert.setTitle("Execute Console Command:");
                    alert.setMessage("Command:");

                    // Set an EditText view to get user input
                    final EditText input = new EditText(ConnectSuccessActivity.this);
                    alert.setView(input);

                    alert.setPositiveButton("Execute", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {

                            Intent li2 = new Intent().setAction(ConnectionService.BROADCAST_FILTER).putExtra("COMMAND", "execute_console_command:"+input.getText().toString());
                            sendBroadcast(li2);
                            Toast.makeText(ConnectSuccessActivity.this, "Command executed", Toast.LENGTH_SHORT).show();
                        }
                    });

                    alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            Toast.makeText(ConnectSuccessActivity.this, "Execute command cancelled", Toast.LENGTH_SHORT).show();
                        }
                    });

                    alert.show();
                }
            }
        });

    }
    /*@Override
    public void onBackPressed(){
        //moveTaskToBack(true);

    }*/

    public void updateServerInfo(String s, String ip, String port){

        TextView ipport = (TextView) findViewById(R.id.ipporttextview);
        ipport.setText(ip+":"+port);

        Log.e("server:", s);

        //motd:currentPlayers:maxPlayers:serverName:bukkitVersion
        String[] rec = s.split(":");
        //Toast.makeText(this, s, Toast.LENGTH_LONG).show();
        if(rec.length >= 4){
            String MOTD = rec[0];
            String currentPlayers = rec[1];
            String maxPlayers = rec[2];
            String serverName = rec[3];
            String version = rec[4];

            TextView players = (TextView) findViewById(R.id.playerslots);
            String p = "Players: "+System.getProperty("line.separator")+currentPlayers+"/"+maxPlayers;
            players.setText(p);

            TextView versionTV = (TextView) findViewById(R.id.version);
            versionTV.setText("Server Version: "+System.getProperty("line.separator")+version);

            TextView motdTV = (TextView) findViewById(R.id.motd);
            motdTV.setText("MOTD: "+System.getProperty("line.separator")+MOTD);



        }



    }


    private void addDrawerItems() {
        String[] osArray = { "Server Info", "Players", "Console Command" };
        mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, osArray);
        mDrawerList.setAdapter(mAdapter);
    }

    private void setUpDrawer(){
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.drawer_open, R.string.drawer_close) {

            // Called when a drawer has settled in a completely open state.
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle("Manage Server");
                invalidateOptionsMenu();
            }

            //Called when a drawer has settled in a completely closed state.
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu();
            }
        };
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.addDrawerListener(mDrawerToggle);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        // Activate the navigation drawer toggle
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private long mLastShakeTime;
    private static final float SHAKE_THRESHOLD = 3.25f; // m/S**2
    private static final int MIN_TIME_BETWEEN_SHAKES_MILLISECS = 1000;

    @Override
    public void onSensorChanged(SensorEvent event) {
            if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                long curTime = System.currentTimeMillis();
                if ((curTime - mLastShakeTime) > MIN_TIME_BETWEEN_SHAKES_MILLISECS) {

                    float x = event.values[0];
                    float y = event.values[1];
                    float z = event.values[2];

                    double acceleration = Math.sqrt(Math.pow(x, 2) +
                            Math.pow(y, 2) +
                            Math.pow(z, 2)) - SensorManager.GRAVITY_EARTH;
                    Log.d("ff", "Acceleration is " + acceleration + "m/s^2");

                    if (acceleration > SHAKE_THRESHOLD) {
                        mLastShakeTime = curTime;
                        Intent localIntent = new Intent().setAction(ConnectionService.BROADCAST_FILTER).putExtra("COMMAND", "player_details_request");
                        sendBroadcast(localIntent);
                        Intent i = new Intent(ConnectSuccessActivity.this, PLActivity.class);
                        startActivity(i);

                    }
                }
            }


    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
