package me.cbartlett.minemanager;

import android.app.Activity;
import android.app.IntentService;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.Context;
import android.content.IntentFilter;
import android.os.IBinder;
import android.os.StrictMode;
import android.provider.ContactsContract;
import android.provider.SyncStateContract;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import me.cbartlett.minemanager.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

public class ConnectionService extends Thread {

    public static final String BROADCAST_FILTER = "40220096.cbartlett.MineManager";

    Socket socket;
    Context context;
    MainActivity ma;
    String port;
    String ip;
    ProgressDialog pd;

    public ConnectionService(Context context) {
        super("TEST");
        this.context = context;
        ma = ((MainActivity) context);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
    }

    @Override
    public void run() {
        //ma.showProgress(true);
        ma.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                pd = new ProgressDialog(context);
                pd.setMessage("Authenticating...");
                pd.show();
            }
        });

        boolean succeed = true;
        try {
            Thread.sleep(2000);
            EditText et = (EditText)((Activity)context).findViewById(R.id.editText);
            EditText et2 = (EditText)((Activity)context).findViewById(R.id.editText2);
            ip = et.getText().toString();
            port = et2.getText().toString();
            int newport = Integer.parseInt(port);
            Log.e("blah", "before socket");
            socket = new Socket(ip, newport);
            Log.e("blah", "after socket"+ip+":"+port);
            Intent i = new Intent(context, ConnectSuccessActivity.class);
            context.startActivity(i);
            //((Activity) context).finish();
            ma.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    pd.dismiss();
                }
            });

        } catch (Exception e) {
            Log.e("-------", Log.getStackTraceString(e));
            //Toast.makeText(context, "Error connecting to server", Toast.LENGTH_SHORT).show();
            //EditText et = (EditText)((Activity) context).findViewById(R.id.editText);
            //  et.requestFocus();

            ma.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    pd.dismiss();
                    EditText et = (EditText)((Activity) context).findViewById(R.id.editText);
                    et.requestFocus();
                    //((MainActivity)context).showProgress(false);
                    Toast.makeText(context, "Login Unsuccessful", Toast.LENGTH_SHORT).show();
                }
            });


        }

       IntentFilter if1 = new IntentFilter(BROADCAST_FILTER);
        this.context.registerReceiver(broadcastReceiver, if1);
    }



    public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //Toast.makeText(context, "Message", Toast.LENGTH_LONG).show();

            if (intent != null) {
                final String action = intent.getAction();
                String command = intent.getStringExtra("COMMAND");
                if (command.equals("execute")) {
                    Log.e("---------", "it worked");
                    try {
                        //Toast.makeText(context, "Message2", Toast.LENGTH_LONG).show();
                        new PrintStream(socket.getOutputStream()).println("server_status_request");

                        BufferedReader bf = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                        String received = bf.readLine();


                        Context c = DataManagerSingleton.getInstance().getConnectSuccessActivityContext();
                        if(c != null){
                            ((ConnectSuccessActivity) c).updateServerInfo(received, ip, port);
                        }


                    } catch (Exception e) {
                        Log.e("---", Log.getStackTraceString(e));
                        ((MainActivity)context).showProgress(false);
                    }
                }
                if (command.equals("player_details_request")) {
                    Log.e("---------", "it worked");
                    try {
                        //Toast.makeText(context, "Message2", Toast.LENGTH_LONG).show();
                        new PrintStream(socket.getOutputStream()).println(command);

                        BufferedReader bf = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                        String received = bf.readLine();

                        //Format:
                        //playerName:playerUUID:playtimeMins:locationWorld:locationX:locationY:locationZ:healthPoints

                        String[] data = received.split(",");
                        if(data.length > 0){
                            DataManagerSingleton.getInstance().getPlayerList().clear();
                            for(int i=0; i<data.length;i++){
                                String[] pData = data[i].split(":");
                                if(pData.length >= 8){
                                    Player p = new Player();
                                    p.setName(pData[0]);
                                    p.setUUID(pData[1]);
                                    p.setTimePlayed(Long.parseLong(pData[2]));
                                    p.setWorldName(pData[3]);
                                    p.setX(Integer.parseInt(pData[4]));
                                    p.setY(Integer.parseInt(pData[5]));
                                    p.setZ(Integer.parseInt(pData[6]));
                                    p.setHealthPoints(Double.parseDouble(pData[7]));

                                    DataManagerSingleton.getInstance().getPlayerList().add(p);
                                }
                            }

                        }



                    } catch (Exception e) {
                        Log.e("---", Log.getStackTraceString(e));

                    }
                }
                if(command.contains(":")){

                        try {
                            //Toast.makeText(context, "Message2", Toast.LENGTH_LONG).show();
                            new PrintStream(socket.getOutputStream()).println(command);
                        } catch (Exception e) {
                            Log.e("---", Log.getStackTraceString(e));
                        }



                }
            }
        }
    };
}
