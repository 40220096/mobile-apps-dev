package me.cbartlett.minemanager;


import android.content.Context;

import java.net.Socket;
import java.util.ArrayList;

/**
 * Created by chris on 11/03/2017.
 */

public class DataManagerSingleton {

    public static DataManagerSingleton _instance = new DataManagerSingleton();

    private ArrayList<Player> playerList = new ArrayList<>();

    public ArrayList<Player> getPlayerList(){
        return this.playerList;
    }

    Player p = new Player();



    private ConnectionService cs;

    //ConnectSuccessActivity context
    private Context csa;

    private DataManagerSingleton(){

    }

    public static DataManagerSingleton getInstance(){
        return _instance;
    }

    public ConnectionService getConnectionService(){
        return this.cs;
    }

    public void setConnectSuccessActivityContext(Context csa){
        this.csa = csa;
    }

    public Context getConnectSuccessActivityContext(){
        return this.csa;
    }

}
