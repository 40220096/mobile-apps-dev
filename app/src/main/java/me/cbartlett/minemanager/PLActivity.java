package me.cbartlett.minemanager;

import android.app.ListActivity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.provider.ContactsContract;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PLActivity extends ListActivity implements PopupMenu.OnMenuItemClickListener{

    private Player p = null;



    @Override
    protected void onCreate(Bundle savedInstanceState) {


        //Intent localIntent = new Intent().setAction(ConnectionService.BROADCAST_FILTER).putExtra("COMMAND", "player_details_request");
        //sendBroadcast(localIntent);

        ImageView im = (ImageView) findViewById(R.id.PLAimageView);
        //Picasso.with(PLActivity.this).load("https://crafatar.com/avatars/4f80d7f9-664a-41aa-9b26-7922b8ba8c5a.png").into(im);


        ArrayList<String> blah = new ArrayList<>();
        blah.clear();
        for(Player p : DataManagerSingleton.getInstance().getPlayerList()){
            blah.add(p.getName());
        }

        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_pl);
        setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, blah));


    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        ListView myListView = getListView();
        Object itemClicked = myListView.getAdapter().getItem(position);


        p = DataManagerSingleton.getInstance().getPlayerList().get((int)id);

        if(p!=null){
            PopupMenu pop = new PopupMenu(this, view);
            pop.setOnMenuItemClickListener(PLActivity.this);
            MenuInflater inf = pop.getMenuInflater();
            inf.inflate(R.menu.player_popup, pop.getMenu());
            pop.show();
            ImageView im = (ImageView) findViewById(R.id.PLAimageView);
            //Picasso.with(PLActivity.this).load("https://crafatar.com/avatars/"+p.getUUID().replace(" ", "")+".png").into(im);
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem menu){
        Log.i("fwf", menu.getItemId()+"");

        switch (menu.getItemId()){

            case R.id.kickPlayer:
                Intent localIntent = new Intent().setAction(ConnectionService.BROADCAST_FILTER).putExtra("COMMAND", "execute_console_command:kick "+p.getName());
                sendBroadcast(localIntent);
                Toast.makeText(this, p.getName()+" was kicked", Toast.LENGTH_SHORT).show();
                return true;

            case R.id.banPlayer:
                Intent li = new Intent().setAction(ConnectionService.BROADCAST_FILTER).putExtra("COMMAND", "execute_console_command:ban "+p.getUUID());
                sendBroadcast(li);
                Toast.makeText(this, p.getName()+" was kicked and banned.", Toast.LENGTH_SHORT).show();
                return true;

            case R.id.msgPlayer:

                // Alert dialog so user can send player a message
                    AlertDialog.Builder alert = new AlertDialog.Builder(this);

                    alert.setTitle("Send to: "+p.getName());

                     //Context context = mapView.getContext();
                    LinearLayout layout = new LinearLayout(this);
                    layout.setOrientation(LinearLayout.VERTICAL);

                    // Set an EditText view to get user input
                    final EditText input = new EditText(this);
                    input.setHint("Message");
                    ImageView im = new ImageView(this);
                    Picasso.with(this).load("https://crafatar.com/avatars/"+p.getUUID().replace(" ", "")+".png").into(im);
                    layout.addView(im);
                    layout.addView(input);

                    alert.setView(layout);


                    alert.setPositiveButton("Send", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {

                            Intent li2 = new Intent().setAction(ConnectionService.BROADCAST_FILTER).putExtra("COMMAND", "send_player_message:"+p.getUUID()+":"+input.getText().toString());
                            sendBroadcast(li2);
                            Toast.makeText(PLActivity.this, "Message was sent to "+p.getName(), Toast.LENGTH_SHORT).show();
                        }
                    });

                    alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            Toast.makeText(PLActivity.this, "Send message cancelled", Toast.LENGTH_SHORT).show();
                        }
                    });

                    alert.show();
                return true;

            case R.id.copyPlayerUUID:
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("UUID", p.getUUID());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(PLActivity.this, p.getName()+"'s UUID copied to clipboard. \n\n "+p.getUUID(), Toast.LENGTH_SHORT).show();
                return true;

            default:
                return true;

        }




    }

}
