package me.cbartlett.minemanager;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * Created by chris on 20/03/2017.
 */

public class PlayerListAdaptor extends ArrayAdapter<String>{

    private final Activity context;
    private final String[] web;
    private final Integer[] imageId;
    public PlayerListAdaptor(Activity context,
                      String[] web, Integer[] imageId) {
        super(context, R.layout.playerlistview, web);
        this.context = context;
        this.web = web;
        this.imageId = imageId;

    }
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.playerlistview, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);

        ImageView imageView = (ImageView) rowView.findViewById(R.id.img);
        //txtTitle.setText(web[position]);

        txtTitle.setText(DataManagerSingleton.getInstance().getPlayerList().get(position).getName());

        //imageView.setImageResource(imageId[position]);
        Picasso.with(context).load("https://crafatar.com/avatars/"+DataManagerSingleton.getInstance().getPlayerList().get(position).getUUID()+".png").into(imageView);
        return rowView;
    }
}